import React from 'react';
import { Text, View, TouchableOpacity, Dimensions, Image } from 'react-native';
import { Camera, Permissions, FaceDetector } from 'expo';
import axios from 'axios';
const http = require("./http");

var {width, height} = Dimensions.get("window");

export default class App extends React.Component {
  state = {
    hasCameraPermission: null,
    faceid: "",
    image: ""
  };

  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
  }
Visagedetecter = async (Faces) => {
  if(Faces.faces[0] != undefined){
    if((Faces.faces[0].faceID != this.state.faceid) ){
      this.setState({faceid: Faces.faces[0].faceID})
      // let photo = await this.camera.takePictureAsync({base64: true});
      const data ={
        tes: "test"
      }
      const res = await PostAPI ("test"," data")
      console.log(res)
      this.setState({
        image: photo.uri
      })

    }
  }


}
  render() {
    const { hasCameraPermission } = this.state;
    if (hasCameraPermission === null) {
      return <View />;
    } else if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Camera style={{ flex: 1 }} 
          ref={ref => { this.camera = ref; }}
          type="front"
          faceDetectorSettings= {{
            mode: FaceDetector.Constants.Mode.accurate,
            detectLandmarks: FaceDetector.Constants.Landmarks.none,
            runClassifications: FaceDetector.Constants.Classifications.all
          }}
          onFacesDetected={this.Visagedetecter}

          >
            <View
              style={{
                flex: 1,
                backgroundColor: 'transparent',
                flexDirection: 'row',
                justifyContent: 'center',
                alignContent: "center",
                alignItems: "center"
              }}>
              <View 
              style={{
                width:  0.75*width,
                height: 0.75*width,
                borderRadius: 0.375*width,
                backgroundColor: "transparent",
                borderWidth: 10, 
                borderColor: "white"
              }}
              >
              <Image source={{uri: this.state.image}} style={{width: 400, height: 400}} />

              </View>

            </View>
          </Camera>
        </View>
      );
    }
  }
}