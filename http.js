import axios from 'axios';

const Link = 'http://192.168.1.13:3000/';

PostAPI = async (path, data) =>{
    try {
    const response = await axios.post(`${Link}${path}`, data, {headers : {"content-type": "multipart/form-data"}});
        return response.data;
    }
    catch(err){
        console.log(err);
    }
}